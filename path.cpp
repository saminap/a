
#include "path.hpp"

Path::Path(const Grid &grid): grid(grid){
}

std::list<Node> Path::findPath(const Node *start, const Node *target, bool cornerCut) const{

	std::list<const Node*> open;
	std::list<const Node*> closed;
	std::list<Node> path;
	std::map<const Node*, NodeInfo> nodeInfo;

	const Node *current;
	const Node *observedNode;
	NodeInfo startNodeInfo;

	startNodeInfo.g = startNodeInfo.h = startNodeInfo.f = 0;
	startNodeInfo.parent = 0;

	open.push_back(start);
	nodeInfo.insert(std::make_pair(start, startNodeInfo));

	if(!grid.getNode(start->getX(), start->getY()) || !grid.getNode(target->getX(), target->getY()))
		return path;

	while(open.size()){

		std::list<const Node*>::iterator currentIter = lowestFValue(open, nodeInfo);
		current = *currentIter;

		if(*current == *target)
			break;

		open.erase(currentIter);
		closed.push_back(current);

		std::list<const Node*> blockedNodes = nearbyWalls(current);

		for(int y=-1; y<=1; ++y){
			for(int x=-1; x<=1; ++x){

				observedNode = grid.getNode(current->getX()+x, current->getY()+y);

				if(!observedNode)
					continue;

				if(observedNode->getType() == Node::WALL || iteratorToNode(closed, observedNode) != closed.end())
					continue;

				if(!cornerCut && iteratorToNode(blockedNodes, observedNode) != blockedNodes.end())
					continue;

				NodeInfo observedNodeInfo;

				observedNodeInfo.g = calculateDistance(observedNode, current) + nodeInfo.find(current)->second.g;
				observedNodeInfo.h = calculateDistance(observedNode, target);
				observedNodeInfo.f = observedNodeInfo.g + observedNodeInfo.h;
				observedNodeInfo.parent = current;

				if(iteratorToNode(open, observedNode) == open.end()){
					open.push_back(observedNode);
					nodeInfo.insert(std::make_pair(observedNode, observedNodeInfo));
				}
				else {
					std::map<const Node*, NodeInfo>::iterator nodeInfoIter = nodeInfo.find(observedNode);
					if(observedNodeInfo.g < nodeInfoIter->second.g){
						nodeInfoIter->second = observedNodeInfo;
					}
				}
			}
		}
	}

	// If the path could not be found
	if(!(*current == *target))
		return path;

	while(current){
		path.push_back(*current);
		current = nodeInfo.find(current)->second.parent;
	}

	path.reverse();

	return path;
}

std::list<const Node*>::const_iterator Path::iteratorToNode(const std::list<const Node*> &list, const Node *node) const{

	std::list<const Node*>::const_iterator it = list.begin();

	while(it != list.end()){
		if(**it == *node)
			return it;

		++it;
	}

	return list.end();
}

std::list<const Node*>::iterator Path::lowestFValue(std::list<const Node*> &list, const std::map<const Node*, NodeInfo> &nodeInfo) const{

	std::list<const Node*>::iterator it = list.begin();
	std::list<const Node*>::iterator lowest = list.begin();
	NodeInfo lowestInfo = nodeInfo.find(*lowest)->second;

	while(it != list.end()){

		if(nodeInfo.find(*it)->second.f == lowestInfo.f){
			if(nodeInfo.find(*it)->second.h < lowestInfo.h){
				lowest = it;
				lowestInfo = nodeInfo.find(*lowest)->second;
			}
		}
		else if(nodeInfo.find(*it)->second.f < lowestInfo.f){
			lowest = it;
			lowestInfo = nodeInfo.find(*lowest)->second;
		}

		++it;
	}

	return lowest;
}

int Path::calculateDistance(const Node *start, const Node *target) const{

	int sum = 0;
	int distanceX = std::abs((int)(target->getX()-start->getX()));
	int distanceY = std::abs((int)(target->getY()-start->getY()));

	while(distanceX > 0 || distanceY > 0){
		if(distanceX > 0 && distanceY > 0)
			sum += 14;
		else 
			sum += 10;

		--distanceX;
		--distanceY;
	}

	return sum;
}

std::list<const Node *> Path::nearbyWalls(const Node *node) const{

	std::list<const Node*> blocked;
	const Node *observedNode;

	// Check for the walls next to current and above and below the current position
	// so that the route does not cut walls but rather evade them
	// X*X
	// *O*
	// X*X

	for(int y=-1; y<=1; ++y){

		for(int x=((y == -1 || y == 1) ? 0 : -1);
                        x<=((y == -1 || y == 1) ? 0 : 1);
                        x+=2){

			observedNode = grid.getNode(node->getX()+x, node->getY()+y);

			if(!observedNode)
				continue;

			if(observedNode->getType() == Node::WALL){
				if(y == -1 || y == 1){
					if(grid.getNode(node->getX()-1, node->getY()+y))
						blocked.push_back(grid.getNode(node->getX()-1, node->getY()+y));

					if(grid.getNode(node->getX()+1, node->getY()+y))
						blocked.push_back(grid.getNode(node->getX()+1, node->getY()+y));
				}
				else {
					if(grid.getNode(node->getX()+x, node->getY()-1))
						blocked.push_back(grid.getNode(node->getX()+x, node->getY()-1));

					if(grid.getNode(node->getX()+x, node->getY()+1))
						blocked.push_back(grid.getNode(node->getX()+x, node->getY()+1));
				}
			}
		}
	}

	return blocked;
}
