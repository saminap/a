
#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>

class Node{

	public:
		enum TYPE{TRAVERSABLE, WALL};
		Node();
		Node(const unsigned int x, const unsigned int y, TYPE type);
		unsigned int getX() const;
		unsigned int getY() const;
		TYPE getType() const;
		bool operator==(const Node &other) const;
		friend std::ostream& operator<<(std::ostream& stream, const Node &node);

	private:
		unsigned int x;
		unsigned int y;
		TYPE type;
};

#endif
