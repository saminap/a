

CPP=$(wildcard *.cpp)
O=$(CPP:.cpp=.o)

.PHONY: clean default

default: $(O)
	g++ $^

%.o: %.cpp %.hpp
	g++ -c -o $@ $<

clean:
	rm *.o
	rm *.out
