
#include "node.hpp"

Node::Node(): x(0), y(0), type(Node::TRAVERSABLE){
}

Node::Node(const unsigned int x, const unsigned int y, TYPE type): x(x), y(y), type(type){
}

unsigned int Node::getX() const{
	return x;
}

unsigned int Node::getY() const{
	return y;
}

Node::TYPE Node::getType() const{
	return type;
}

bool Node::operator==(const Node &other) const{
	return this->x == other.x &&
               this->y == other.y;
}

std::ostream& operator<<(std::ostream& stream, const Node &node){
	stream << node.x << "," << node.y;
	return stream;
}
