
#include "grid.hpp"

Grid::Grid(){

}

const Node *Grid::getNode(const unsigned int x, const unsigned int y) const{
	try{
		return &nodes.at(y).at(x);
	}
	catch(const std::out_of_range){
		return 0;
	}
}

void Grid::addNode(const unsigned int x, const unsigned int y, Node::TYPE type){

	Node node(x, y, type);

	try{
		nodes.at(y).push_back(node);
	}
	catch(const std::out_of_range& oor){
		std::vector<Node> row;
		row.push_back(node);
		nodes.push_back(row);
	}
}

void Grid::printGrid() const{

	unsigned int rows = nodes.size();

	for(unsigned int y=0; y<rows; ++y){
		for(unsigned int x=0; x<nodes[y].size(); ++x){
			std::cout << nodes.at(y).at(x).getType();
		}
		std::cout << std::endl;
	}
}

void Grid::printGrid(const std::list<Node> &path) const{

	unsigned int rows = nodes.size();

	for(unsigned int y=0; y<rows; ++y){
		for(unsigned int x=0; x<nodes[y].size(); ++x){

			std::list<Node>::const_iterator pathIter = path.begin();
			bool pathOnCoord = false;

			while(pathIter != path.end()){
				if(*pathIter == *getNode(x, y)){
					std::cout << "P";
					pathOnCoord = true;
					break;
				}
				++pathIter;
			}

			if(!pathOnCoord)
				std::cout << nodes.at(y).at(x).getType();
		}
		std::cout << std::endl;
	}
}
