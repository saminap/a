
#ifndef PATH_HPP
#define PATH_HPP

#include "node.hpp"
#include "grid.hpp"

#include <iostream>
#include <map>
#include <list>
#include <cmath>
#include <stdexcept>

struct NodeInfo{
	unsigned int g;
	unsigned int h;
	unsigned int f;
	const Node *parent;
};

class Path{

	public:
		Path(const Grid &grid);
		std::list<Node> findPath(const Node *start, const Node *target, bool cornerCut=false) const;

	private:
		const Grid &grid;

		std::list<const Node*>::const_iterator iteratorToNode(const std::list<const Node*> &list, const Node *node) const;
		std::list<const Node*>::iterator lowestFValue(std::list<const Node*> &list, const std::map<const Node*, NodeInfo> &nodeInfo) const;
		int calculateDistance(const Node *start, const Node *target) const;
		std::list<const Node*> nearbyWalls(const Node *node) const;
};

#endif
