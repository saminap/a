
#ifndef GRID_HPP
#define GRID_HPP

#include "node.hpp"
#include <iostream>
#include <vector>
#include <list>
#include <stdexcept>

class Grid{

	public:
		Grid();
		const Node *getNode(const unsigned int x, const unsigned int y) const;
		void addNode(const unsigned int x, const unsigned int y, Node::TYPE type);
		void printGrid() const;
		void printGrid(const std::list<Node> &path) const;

	private:
		std::vector< std::vector<Node> > nodes;
};

#endif
